### Upgrade control plane node

##### update apt repository
    vim /etc/apt/sources.list.d/kubernetes.list

##### 
    sudo apt-mark unhold kubeadm
    sudo apt-get update
    sudo apt-get install -y kubeadm=1.x.x-x.x
    sudo apt-mark hold kubeadm

##### get upgrade preview
    sudo kubeadm upgrade plan

##### upgrade cluster 
    sudo kubeadm upgrade apply v1.29.0

##### drain node
    kubectl drain master --ignore-daemonsets

##### upgrade kubelet & kubectl 
    sudo apt-mark unhold kubelet kubectl
    sudo apt-get update
    sudo apt-get install -y kubelet=1.x.x-x.x kubectl=1.x.x-x.x
    sudo apt-mark hold kubelet kubectl

##### restart kubelet
    sudo systemctl daemon-reload
    sudo systemctl restart kubelet

##### uncordon node
    kubectl uncordon master


### Upgrade worker node
    sudo apt-mark unhold kubeadm 
    sudo apt-get update
    sudo apt-get install -y kubeadm=1.x.x-x.x 
    sudo apt-mark hold kubeadm

    sudo kubeadm upgrade node

    kubectl drain worker1 --ignore-daemonsets --force

    sudo apt-mark unhold kubelet kubectl 
    sudo apt-get update 
    sudo apt-get install -y kubelet=1.x.x-x kubectl=1.x.x-xx 
    sudo apt-mark hold kubelet kubectl

    sudo systemctl daemon-reload
    sudo systemctl restart kubelet

    kubectl uncordon worker1
